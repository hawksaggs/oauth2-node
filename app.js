require('dotenv').config();
const express = require('express'),
	bodyParser = require('body-parser'),
	logger = require('morgan');


const db = require('./database/mysql');
const helper = require('./helper/helper');
const app = express();

const TokenController = require('./controllers/token');

app.use(bodyParser.urlencoded({ extended: true }));
app.use(logger('dev'));
app.use(bodyParser.json());

app.post('/login', helper.obtainToken);

app.post('/oauth/token', helper.obtainToken);

app.get('/logout', helper.authenticateRequest, TokenController.deleteTokenByAccessToken);

app.get('/user/token/list', helper.authenticateRequest, TokenController.getTokenList);

app.delete('/user/token/:id', helper.authenticateRequest, TokenController.deleteTokenById);

const port = process.env.PORT || 5000;

app.listen(port, function () {
	console.log('Server started... http://localhost:%d', port);
});
