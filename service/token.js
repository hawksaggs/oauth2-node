const Repository = require('../repository/index');
const repository = new Repository("Token");
module.exports = {
    save: (data) => {
        return new Promise((resolve, reject) => {
            repository.save(data)
                .then((token) => {
                    resolve(token);
                })
                .catch((err) => {
                    reject(err);
                });
        });
    },
    getAll: (userId) => {
        return new Promise((resolve, reject) => {
            const repository = new Repository("Token");
            const where = {
                where: {
                    user_id: userId
                }
            };
            repository.findAll(where)
                .then((tokens) => {
                    resolve(tokens);
                })
                .catch((err) => {
                    reject(err);
                });
        });
    },
    deleteById: (id) => {
        return new Promise((resolve, reject) => {
            const where = {
                where: {
                    id: id
                }
            };
            repository.delete(where)
                .then((tokens) => {
                    resolve();
                })
                .catch((err) => {
                    reject(err);
                });
        });
    },
    deleteByAccessToken: (accessToken) => {
        return new Promise((resolve, reject) => {
            const where = {
                where: {
                    access_token: accessToken
                }
            };
            repository.delete(where)
                .then((tokens) => {
                    resolve();
                })
                .catch((err) => {
                    reject(err);
                });
        });
    },
    count: (userId) => {
        return new Promise((resolve, reject) => {
            const where = {
                where: {
                    user_id: userId
                }
            };
            repository.count(where)
                .then((count) => {
                    resolve(count);
                })
                .catch((err) => {
                    reject(err);
                });
        });
    },
    getPaginatedResult: (userId, skip, limit) => {
        return new Promise((resolve, reject) => {
            const repository = new Repository("Token");
            const where = {
                where: {
                    user_id: userId
                },
                offset: skip,
                limit:limit
            };
            repository.findAll(where)
                .then((tokens) => {
                    resolve(tokens);
                })
                .catch((err) => {
                    reject(err);
                });
        });
    },

}