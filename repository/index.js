var db = require('../database/mysql');

class Repository {
    constructor(modelName) {
        this.modelName = modelName;
    }

    findOne(query) {
        return new Promise((resolve, reject) => {
            db[this.modelName].findOne(query)
                .then(function (data) {
                    if (!data) {
                        reject('Does not exists');
                    }
                    resolve(data);
                })
                .catch(function (err) {
                    reject(err);
                });
        });
    }

    findAll(query) {
        return new Promise((resolve, reject) => {
            db[this.modelName].findAll(query)
                .then(function (data) {
                    resolve(data);
                })
                .catch(function (err) {
                    reject(err);
                });
        });
    }

    save(data) {
        return new Promise((resolve, reject) => {
            const dbObj = db[this.modelName].build(data);
             dbObj.save()
                 .then(function () {
                     resolve();
                 })
                 .catch(function (err) {
                     reject(err);
                 });
        });
    }

    delete(query) {
        return new Promise((resolve, reject) => {
            db[this.modelName].destroy(query)
                .then(function (token) {
                    resolve();
                })
                .catch(function (err) {
                    reject(err);
                });
        });
    }

    count(query) {
        return new Promise((resolve, reject) => {
            db[this.modelName].findAndCountAll(query)
                .then(function (result) {
                    resolve(result.count);
                })
                .catch(function (err) {
                    reject(err);
                });
        });
    }
}

module.exports = Repository;