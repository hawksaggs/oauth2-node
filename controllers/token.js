const tokenService = require('../service/token');

module.exports = {
    getTokenList: (req, res) => {
        const size = req.query.size > 0 ? parseInt(req.query.size) : 10 || 10;
        const page = req.query.page > 0 ? parseInt(req.query.page) : 1 || 1;
        const skip = (page - 1) * size;
        const userId = res.locals.oauth.token.user.id;
        Promise.all([
            tokenService.count(userId),
            tokenService.getPaginatedResult(userId, skip, size)
        ])
            .then(([count, data]) => {
                return res.status(200).json({
                    success: true,
                    data: {
                        totalPage: Math.ceil(count / size),
                        currentPage: page,
                        data:data
                    }
                });
            })
            .catch((err) => {
                return res.status(400).json({
                    success: false,
                    error: {
                        key: "operationFailed"
                    }
                });
            });
    },
    deleteTokenById: (req, res) => {
        const id = req.params.id;
        tokenService.deleteById(id)
            .then(() => {
                return res.status(200).json({
                    success: true,
                    data: []
                });
            })
            .catch((err) => {
                return res.status(400).json({
                    success: false,
                    error: {
                        key: "operationFailed"
                    }
                });
            });
    },
    deleteTokenByAccessToken: (req, res) => {
        const accessToken = res.locals.oauth.token.accessToken;
        tokenService.deleteByAccessToken(accessToken)
            .then(() => {
                return res.status(200).json({
                    success: true,
                    data: []
                });
            })
            .catch((err) => {
                return res.status(400).json({
                    success: false,
                    error: {
                        key: "operationFailed"
                    }
                });
            });
    }
};