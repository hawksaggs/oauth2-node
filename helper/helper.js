const OAuth2Server = require('oauth2-server'),
    Request = OAuth2Server.Request,
    Response = OAuth2Server.Response,
    oauth = require('../oauth/oauth').oauth(),
    crypto = require('crypto');

exports.obtainToken = (req, res) => {

    var request = new Request(req);
    var response = new Response(res);

    return oauth.token(request, response)
        .then(function (token) {

            res.json(token);
        }).catch(function (err) {

            res.status(err.code || 500).json(err);
        });
}

exports.authenticateRequest = (req, res, next) => {

    var request = new Request(req);
    var response = new Response(res);

    return oauth.authenticate(request, response)
        .then(function (token) {
            res.locals.oauth = {
                token:token
            };
            next();
        }).catch(function (err) {
            res.status(err.code || 500).json(err);
        });
}

exports.generateHash = (str) => {
    const secret = process.env.PASSWORD_HASH_KEY;
    const hash = crypto.createHmac('sha256', secret)
        .update(str)
        .digest('hex');
    
    return hash;
}