var Sequelize = require('sequelize');
var DataTypes = Sequelize;

const db = new Sequelize({
    database: process.env.MYSQL_DATABASE,
    host: process.env.MYSQL_HOST,
    port: process.env.MYSQL_PORT,
    username: process.env.MYSQL_USER,
    password: process.env.MYSQL_PASSWORD,
    dialect: process.env.SEQUELIZE_DIALECT,
    operatorsAliases: false,
    pool: {
        max: 5,
        min: 0,
        acquire: 30000,
        idle: 10000
    }
});

db.User = require('../models/user')(db, DataTypes);
db.Token = require('../models/token')(db, DataTypes);
db.Client = require('../models/client')(db, DataTypes);
db.Grants = require('../models/grants')(db, DataTypes);

// drop the entire db when on a local machine and run the test files in the test/ dir
// if (!process.env.NODE_ENV) {
//     db.drop();
// }

db.sync()
    .then(function () {
        console.log('connected :)');
    })
    .catch(function (err) {
        console.log('connection failed...', err);
    });

module.exports = db;
