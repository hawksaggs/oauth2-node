const OAuth2Server = require('oauth2-server');

exports.oauth = () => {
    return new OAuth2Server({
        model: require('./model.js'),
        accessTokenLifetime: process.env.ACCESS_TOKEN_EXPIRY_TIME,
        refreshTokenLifetime: process.env.REFRESH_TOKEN_EXPIRY_TIME,
        allowBearerTokensInQueryString: true
    });
}