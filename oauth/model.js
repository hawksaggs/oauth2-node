const db = require('../database/mysql');
const helper = require('../helper/helper');
const Token = require('../service/token');

exports.getAccessToken = function (bearerToken, callback) {
    var self = this;
    db.Token.findOne({ where: { access_token: bearerToken } })
        .then(function (token) {
            if (!token) {
                callback('tokenExpired');
            }
            return Promise.all([
                token,
                self.getClientFromClientId(token.client_id),
                self.getUserFromUserId(token.user_id)
            ])
        })
        .spread(function (token, client, user) {
            callback(null, {
                accessToken: token.access_token,
                accessTokenExpiresAt: token.access_token_expires,
                scope: token.scope,
                client: client, // with 'id' property
                user: user
            });
        })
        .catch(function (err) {
            // console.log('err', err);
            callback(err);
        });
};

exports.getRefreshToken = function (refreshToken, callback) {
    var self = this;
    db.Token.findOne({
            where: {
                refresh_token: refreshToken
            }
        })
        .then(function (token) {
            if (!token) {
                callback('refreshTokenExpired');
            }

            return Promise.all([
                token,
                self.getClientFromClientId(token.client_id),
                self.getUserFromUserId(token.user_id)
            ])
        })
        .spread(function (token, client, user) {
            callback(null, {
                client: client,
                refreshToken: token.refresh_token,
                refreshTokenExpiresAt: token.refresh_token_expires,
                user: user
            });
        })
        .catch(function (err) {
            callback(err);
        });
};

exports.getClient = function (clientId, clientSecret, callback) {
    var where = {
        client_id: clientId,
        client_secret: clientSecret
    };
    db.Client.findOne({ where: where })
        .then(function (client) {
            if (!client) {
                callback('clientIdNotFound');
            }
            // console.log(client);
            client.grants = ['password', 'refresh_token', 'client_credentials'];
            callback(null, client);
        })
        .catch(function (err) {
            callback(err);
        });
};

exports.getUser = function (username, password, callback) {
    var where = {
        email: username,
        password: helper.generateHash(password)
    };
    db.User.findOne({
            where: where
        })
        .then(function (user) {
            callback(null, user);
        });
};

exports.getUserFromClient = function (clientId, clientSecret, callback) {
    var where = {
        client_id: clientId,
        client_secret: clientSecret
    };
    db.Client.findOne({ where: where })
        .then(function (client) {
            if (!client) {
                callback('invalidClientId');
            }
            db.User.find(client.user_id)
                .then(function (user) {
                    callback(false, user);
                });
        });
};

exports.saveToken = (token, client, user, callback) => {
    var data = {
        accessToken: token.accessToken,
        accessTokenExpiresAt: token.accessTokenExpiresAt,
        refreshToken: token.refreshToken,
        refreshTokenExpiresAt: token.refreshTokenExpiresAt,
        client: {
            id: client.id
        },
        user: {
            id: user.id
        }
    };
    const dataToBeSave = {
        access_token: token.accessToken,
        access_token_expires: token.accessTokenExpiresAt,
        refresh_token: token.refreshToken,
        refresh_token_expires: token.refreshTokenExpiresAt,
        client_id: client.id,
        user_id: user.id
    };
    Token.save(dataToBeSave)
        .then((token) => {
            callback(null, data);
        })
        .catch((err) => {
            callback(err);
        });
};

exports.revokeToken = function (token, callback) {
    db.Token.destroy({
        where: { refresh_token: token.refreshToken }
        })
        .then(function (token) {
            if (!token) {
                callback(null, false);
            }
            callback(null, true);
        })
        .then(function () {
            callback(null, true);
        })
        .catch(function (err) {
            callback(err);
        })
};

exports.getUserFromUserId = (userId) => {
    return new Promise((resolve, reject) => {
        db.User.findOne({
            where: { id: userId }
        })
            .then((user) => {
                resolve(user);
            })
            .catch((err) => {
                reject(err);
            });
    });

}

exports.getClientFromClientId = (clientId) => {
    return new Promise((resolve, reject) => {
        db.Client.findOne({
            where: { id: clientId }
        })
            .then((client) => {

                resolve(client);
            })
            .catch((err) => {
                reject(err);
            });
    });

}   