module.exports = function (db, DataTypes) {
    return db.define('clients', {
        client_id: {
            type: DataTypes.STRING,
            unique: true
        },
        client_secret: DataTypes.STRING,
        client_name: DataTypes.STRING,
        redirect_uri: DataTypes.STRING,
        created_at: {
            type: 'TIMESTAMP',
            defaultValue: DataTypes.literal('CURRENT_TIMESTAMP'),
            allowNull: false
        },
        updated_at: {
            type: 'TIMESTAMP',
            defaultValue: DataTypes.literal('CURRENT_TIMESTAMP'),
            allowNull: false
        }
    }, {
            timestamps: false
        });
};
