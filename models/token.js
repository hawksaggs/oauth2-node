module.exports = function (db, DataTypes) {
    return db.define('tokens', {
        access_token: DataTypes.STRING,
        access_token_expires: DataTypes.DATE,
        refresh_token: DataTypes.STRING,
        refresh_token_expires: DataTypes.DATE,
        client_id: DataTypes.STRING,
        user_id: {
            type: DataTypes.INTEGER
        },
        created_at: {
            type: 'TIMESTAMP',
            defaultValue: DataTypes.literal('CURRENT_TIMESTAMP'),
            allowNull: false
        },
        updated_at: {
            type: 'TIMESTAMP',
            defaultValue: DataTypes.literal('CURRENT_TIMESTAMP'),
            allowNull: false
        }
    }, {
            timestamps: false
        });
};
