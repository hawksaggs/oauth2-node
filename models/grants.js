module.exports = function (db, DataTypes) {
    return db.define('grants', {
        grant_type: DataTypes.STRING,
        created_at: {
            type: 'TIMESTAMP',
            defaultValue: DataTypes.literal('CURRENT_TIMESTAMP'),
            allowNull: false
        },
        updated_at: {
            type: 'TIMESTAMP',
            defaultValue: DataTypes.literal('CURRENT_TIMESTAMP'),
            allowNull: false
        }
    }, {
            timestamps: false
        });
};
